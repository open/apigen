package main

import (
	api "go.digitalcircle.com.br/tools/apigen/test/demo/cli"
	"log"
	"net/http"
	"time"
)

func main() {
	api.Host = "http://localhost:8080"
	api.SetCli(&http.Client{})
	res, err := api.Dosome(&api.SomeReq{
		Fielda: "ASD123",
		Fieldb: time.Unix(0, 0),
	})
	if err != nil {
		panic(err)
	}
	log.Printf("%#v", res)
}
