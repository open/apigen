package api

import (
	"context"
	"fmt"
	"time"
)

//@API
type SomeReq struct {
	Fielda string    `json:"fielda"`
	Fieldb time.Time `json:"fielda"`
}

//@API
type SomeReq2 struct {
	Fielda string    `json:"fielda"`
	Fieldb time.Time `json:"fielda"`
}

//@API
type SomeRes struct {
	Msg string `json:"msg"`
}

//@API
//@PATH: /some
//@VERB: POST
func Dosome(ctx context.Context, req *SomeReq) (res *SomeRes, err error) {
	res = &SomeRes{Msg: fmt.Sprintf("%s: %s", req.Fielda, req.Fieldb.String())}
	return
}

//@API
//@PATH: /some2
//@VERB: POST
func Dosome2(ctx context.Context, req *SomeReq2) (res *SomeRes, err error) {
	res = &SomeRes{Msg: fmt.Sprintf("%s: %s", req.Fielda, req.Fieldb.String())}
	return
}
