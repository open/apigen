package main

import (
	"go.digitalcircle.com.br/tools/apigen/test/demo/api"
	"net/http"
)

func main() {
	aapi := api.Init()
	http.ListenAndServe(":8080", aapi.Mux)
}
