from dataclasses import dataclass
import requests
import json
from typing import List
#region Base


__ctx = {"apibase":""}


def SetAPIBase(s: str):
    __ctx["apibase"] = s


def GetAPIBase() -> str:
    return __ctx["apibase"]


def SetCookie(s: str):
    __ctx["cookie"] = s


def GetCookie() -> str:
    return __ctx["cookie"]


def InvokeTxt(path: str, method: str, body) -> str:
    headers = {"Content-type": "application/json", "Cookie": "dc="+GetCookie()}
    fpath = GetAPIBase() + path
    r = requests.request(method, fpath, json=body, headers=headers)
    return r.text


def InvokeJSON(path: str, method: str, body) -> dict:
	d = body.__dict__
	return json.loads(InvokeTxt(path, method, d))

#endregion

#region Types
@ dataclass
class SomeReq :
	fielda: str


@ dataclass
class SomeRes :
	msg: str


#endregion

#region Methods
def Dosome(req:SomeReq)-> SomeRes:
	return InvokeJSON("/some","POST",req)


#endregion
