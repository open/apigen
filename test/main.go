package main

import (
	"github.com/gin-gonic/gin"
	"go.digitalcircle.com.br/tools/apigen/test/goapi"
)

func main() {
	g := gin.Default()
	goapi.Build(g)
	g.Run()
}
