package main

import (
	"fmt"
	"go.digitalcircle.com.br/open/apigen/lib"
	"os"
)

var Ver string = "2021-11-28T13:57:28.822468Z"

func main() {
	os.Stdout.WriteString(fmt.Sprintf("Version: %s\n", Ver))
	lib.Run()
}
