module go.digitalcircle.com.br/open/apigen

go 1.17

require (
	github.com/alecthomas/kong v0.2.17
	github.com/fatih/structtag v1.2.0
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v2 v2.4.0
)
