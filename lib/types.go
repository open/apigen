package lib

type API struct {
	BasePath             string                `yaml:"basepath,omitempty"`
	Host                 string                `yaml:"host,omitempty"`
	Types                map[string]*APIType   `yaml:"types,omitempty"`
	Methods              map[string]*APIMethod `yaml:"methods,omitempty"`
	Namespace            string                `yaml:"namespace"`
	Imports              map[string]string     `yaml:"imports"`
	UsedImportsTypes     map[string]string     `yaml:"used_imports_types"`
	UsedImportsFunctions map[string]string     `yaml:"used_imports_functions"`
	SortedPaths          []*APIPath            `yaml:"-"`
	Paths                map[string]*APIPath   `yaml:"paths"`
}

type APIPath struct {
	Path        string              `yaml:"path"`
	MapVerbs    map[string]*APIVerb `yaml:"map_verbs"`
	SortedVerbs []*APIVerb          `yaml:"sorted_verbs"`
}

type APIVerb struct {
	Verb   string     `yaml:"verb"`
	Method *APIMethod `yaml:"method"`
}
type APIFieldTag struct {
	Key  string   `yaml:"key"`
	Name string   `yaml:"name"`
	Opts []string `yaml:"opts"`
}
type APIField struct {
	Type   string                 `yaml:"type,omitempty"`
	Array  bool                   `yaml:"array,omitempty"`
	Desc   string                 `yaml:"desc,omitempty"`
	Map    bool                   `yaml:"map,omitempty"`
	Mapkey string                 `yaml:"mapkey,omitempty"`
	Mapval string                 `yaml:"mapval,omitempty"`
	Tags   map[string]APIFieldTag `yaml:"tags,omitempty"`
}

func (a *APIField) String() string {
	if a.Array {
		return "[]" + a.Type
	} else {
		return a.Type
	}
}

type APIType struct {
	Name    string               `yaml:"name,omitempty"`
	Desc    string               `yaml:"desc,omitempty"`
	Fields  map[string]*APIField `yaml:"fields,omitempty"`
	Col     string               `yaml:"col,omitempty"`
	TypeDef string               `yaml:"-"`
}

type APIParamType struct {
	Typename  string
	Ispointer bool
	IsArray   bool
}

type APIMethod struct {
	Name    string `yaml:"name"`
	Desc    string `yaml:"desc"`
	Verb    string `yaml:"verb"`
	Path    string `yaml:"path"`
	Perm    string `yaml:"perm"`
	Raw     bool   `yaml:"raw"`
	OpID    string `yaml:"op_id"`
	ReqType *APIParamType
	ResType *APIParamType
}

func APIParamTypeToString(t *APIParamType) string {
	ret := ""
	if t.IsArray {
		ret = "[]"
		if t.Ispointer {
			ret = ret + "*"
		}
		ret = ret + t.Typename
		return ret
	}
	if t.Ispointer {
		ret = ret + "*"
	}
	ret = ret + t.Typename
	return ret
}

func APIParamTypeDecToString(t *APIParamType) string {
	ret := ""
	if t.IsArray {
		ret = "[]"
		if t.Ispointer {
			ret = ret + "*"
		}
		ret = ret + t.Typename
		return ret
	}
	if t.Ispointer {
		ret = ret + "*"
	}
	ret = ret + t.Typename
	return ret
}

func APIParamTypeUseRef(t *APIParamType) string {
	if t.IsArray || !t.Ispointer {
		return "&"
	}
	return ""
}
